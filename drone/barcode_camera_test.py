import numpy as np
import cv2
from bardecoder import *

camera = cv2.VideoCapture(1)
decoder = Decoder()

while True:
    # grab the current frame
    (grabbed, frame) = camera.read()
    barcodes = decoder.decode(frame)
    for i in barcodes:
        print(i.value)
        cv2.polylines(frame,[np.int32(i.location)],True,255,3, cv2.LINE_AA)
    cv2.imshow("frame",frame)
    if cv2.waitKey(1) & 0xFF == ord("q"):
        break
cv2.destroyAllWindows()
camera.release()
